#include <iostream>
#include <dirent.h>
#include <vector>
#include <winbase.h>
#include <regex>

int main() {
    std::cout << "Discord Cache Clear v1.1 by luaq.\r\n";
    std::cout << "\r\nClearing Cache...\r\n";

    const char *app_data = getenv("appdata");
    std::string cache_folder = std::string(app_data) + "\\discord\\Cache";

    Sleep(2000);

    DIR *cache_directory;
    struct dirent *entry;
    if ((cache_directory = opendir(cache_folder.c_str())) != NULL) {
        while ((entry = readdir(cache_directory)) != NULL) {
            std::string file_name = entry->d_name;
            if(regex_match(file_name, std::regex("\.\.?"))) continue;
            std::string full_path = cache_folder + "\\" + file_name;
            std::cout << "Deleting " << file_name << "\r\n";
            if (remove(full_path.c_str()) != 0) {
                continue;
            }
            std::cout << "Deleted.\r\n";
        }
        std::cout << "Finished clearing.\r\n";
    } else {
        std::cout << "No Cache Folder Found\r\n";
    }

    system("pause >nul");

    return 0;
}